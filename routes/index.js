var express = require('express');
var router  = express.Router();

/**
 *
 * @param {number} n
 * @returns {number}
 */
function fact(n) {
    var res = 1;
    for (var i = 1; i < n; i++) {
        res *= i;
    }
    return res;
}

/**
 *
 * @param {number} n
 * @returns {number}
 */
function random(n) {
    return Math.round(Math.random() * n);
}

router.get('/a', (req, res, next) => {
    res.send({value: fact(random(100))})
});

router.get('/b', (req, res, next) => {
    setTimeout(() => res.send({value: random(1000000)}), random(60000));
});

router.get('/c', (req, res, next) => {
    const length = random(100000) + 10000;
    const a = [];
    for (var i=0; i < length;i++) {
        a.push({value: random(100000)});
    }
    res.send(a);
});

module.exports = router;
